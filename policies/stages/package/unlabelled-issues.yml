.common_conditions: &common_conditions
  conditions:
    state: opened
    labels:
      - none

.common_rules: &common_rules
  limits:
    most_recent: 45

.common_actions: &common_actions
  summarize:
    destination: gitlab-org/quality/triage-reports
    item: |
      - [ ] gitlab-org/gitlab##{resource[:iid]} {{title}} {{labels}}
    title: |
      #{Date.today.iso8601} Newly created unlabelled issues requiring initial triage
    summary: |
      Hi Triage Team,

      Here is a list of the latest issues without labels in the project.

      In accordance with the [Partial triage guidelines](https://about.gitlab.com/handbook/engineering/issue-triage/#partial-triage), we would like to ask you to:

      1. Check for duplicates in this project and others (it is common for issues reported in EE to already exist in CE).
      1. Add a [type label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#type-labels).
        - If identified as a bug, add a [severity label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#severity-labels).
        - If the severity is ~S1 or ~S2, [mention relevant PM/EMs from the relevant stage group from product devstages categories](https://about.gitlab.com/handbook/product/categories/#devops-stages).
      1. Add a [group label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#group-labels).
      1. Add a [stage label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#stage-labels).
      1. Add relevant [category](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#category-labels) and [facet](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#facet-labels) labels to facilitate automatic addition of stage and group labels.
      1. If needed, [mention relevant domain experts](https://about.gitlab.com/company/team/structure/#expert) if the issue requires further attention.

      For the issues triaged please check off the box in front of the given issue.

      Once you've triaged all the issues assigned to you, you can unassign yourself with the `/unassign me` quick action.

      **When all the checkboxes are done, close the issue, and celebrate!** :tada:

      #{
      potential_triagers = %w[@rymai @markglenfletcher @godfat @ddavison @mlapierre @at.ramya @sliaquat @tmslvnkc @zeffmorgan @tpazitny @wlsf82 @dchevalier2 @asoborov @grantyoung @jennielouie @kwiebers @niskhakova @caalberts @svistas].shuffle
      list_items = resource[:items].split("\n")
      items_per_triagers = potential_triagers
        .zip(list_items.each_slice((list_items.size.to_f / potential_triagers.size).ceil))
        .to_h.compact

      items_per_triagers.each_with_object([]) do |(triager, items), text|
        text << "#{triager}\n\n#{items.join("\n")}"
      end.join("\n\n")
      }

      /assign #{items_per_triagers.keys.join(' ')}
      /label ~Quality ~"triage\-package"

resource_rules:
  issues:
    rules:
      - name: Collate latest unlabelled issues
        <<: *common_rules
        <<: *common_conditions
        actions:
          <<: *common_actions
