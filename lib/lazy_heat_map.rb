# frozen_string_literal: true

require 'cgi'

class LazyHeatMap
  def initialize(resources, policy_spec)
    @resources = resources
    @policy_spec = policy_spec
  end

  def to_s
    @to_s ||= generate_heat_map_table
  end

  private

  def generate_heat_map_table
    severities = heat_map.each_value.flat_map(&:keys).uniq

    body = heat_map.each_key.map do |priority|
      row = ["~#{priority}"] +
        severities.map do |severity|
          issues_link(priority, severity)
        end

      "| #{row.join(' | ')} |"
    end.join("\n")

    header = "|| #{severities.map { |s| "~#{s}" }.join(' | ')} |"
    separator = "|----|#{severities.map { '----' }.join('|')}|"

    "#{header}\n#{separator}\n#{body}"
  end

  def heat_map
    @heat_map ||= generate_heat_map
  end

  def generate_heat_map
    grouped_by_priority = @resources.group_by do |resource|
      # Pick highest one
      resource[:labels].grep(/\AP\d\z/).sort.first || '"No priority"'
    end

    grouped_by_priority = pad_and_sort(grouped_by_priority, %w[P1 P2 P3 P4])

    grouped_by_priority.transform_values do |with_same_priority|
      grouped_by_severity = with_same_priority.group_by do |resource|
        resource[:labels].grep(/\AS\d\z/).sort.first || '"No severity"'
      end

      pad_and_sort(grouped_by_severity, %w[S1 S2 S3 S4])
    end
  end

  def pad_and_sort(hash, labels)
    labels.each do |name|
      hash[name] ||= {}
    end

    hash.sort_by { |k, _| k[/\d/] || 'Z' }.to_h
  end

  def issues_link(priority, severity)
    count = heat_map.dig(priority, severity)&.size || 0

    if count.zero?
      count
    else
      <<~MARKDOWN.chomp
        [#{count}](#{issues_base_url}?#{issues_query(priority, severity)})
      MARKDOWN
    end
  end

  def issues_query(priority, severity)
    state = [@policy_spec.dig(:conditions, :state)].compact
    labels = @policy_spec.dig(:conditions, :labels) || []

    {
      'state' => state,
      'label_name[]' => labels + [priority, severity],
    }.flat_map do |key, values|
      values.map { |v| "#{CGI.escape(key)}=#{CGI.escape(v)}" }
    end.join('&')
  end

  def issues_base_url
    @issues_base_url ||= @resources.dig(0, :web_url)&.sub(%r{/\d+\z}, '')
  end
end

module IssueBuilderWithHeatMap
  private

  def description_resource
    super.merge(heat_map: LazyHeatMap.new(@resources, @policy_spec))
  end
end
